const jobPostings = [
  {
    _id: 1,
    name: ' Python Programmer',
    lowerBoundAge: 25,
    upperBoundAge: 40,
    gender: 1,
    experience: 2,
    companyId: 1
  },
  {
    _id: 2,
    name: ' Senior Java Programmer',
    lowerBoundAge: 30,
    upperBoundAge: 50,
    gender: 0,
    experience: 2,
    companyId: 2
  },
  {
    _id: 3,
    name: ' Junior Java Programmer',
    lowerBoundAge: 22,
    upperBoundAge: 30,
    gender: 0,
    experience: 3,
    companyId: 1
  },
  {
    _id: 4,
    name: ' Junior Node Programmer',
    lowerBoundAge: 22,
    upperBoundAge: 30,
    gender: 2,
    experience: 2,
    companyId: 1
  }
]
const checkAge = function (age, begin, end) {
  if (age >= begin && age <= end) return true
  return false
}
const checkGender = function (gender, postingGender) {
  if (postingGender === 0) {
    return true
  }
  if (postingGender === gender) return true
  return false
}
const checkExprience = function (experience, postingExperience) {
  if (experience >= postingExperience) return true
  return false
}

const modelGetJobPostingWithId = function (id) {
  return new Promise(function (resolve, reject) {
    if (id <= jobPostings.length) {
      resolve(jobPostings[id - 1])
    } else {
      reject(new Error('No data'))
    }
  })
}

const searchTextJobPosting = function (text) {
  return jobPostings
}

module.exports = {
  checkAge,
  checkGender,
  checkExprience,
  modelGetJobPostingWithId,
  searchTextJobPosting
}
